# This solves the Poisson equation in 1D or 2D (set dims==1 or ==2)
#
# - d/dx( k dphi/dx)) = f
#
# k = 1, f = such that the manufacture solution is the solution:
#
# cos(x)*cos(y)
#
# Boundary conditions are Dirichlet at top and bottom, and either left and right.
#
# Weak form:
# int( grad theta . k grad phi) = int( theta f) + int_b(theta k dphi/dn)

# This file is derived from the sparseFEM package https://bitbucket.org/maurow/sparsefem
# Licensed under a MIT licence

using LMesh
using SparseFEM
using Winston

#function runFEM(nx=10)
nx=100
dims = 1
plotyes = [false, true][2]

# make the manufactured solution and mesh
sq1(x) = squeeze(x, 1)

## Manufactured solution
manusol(coords) =  sq1(prod(map(cos, coords),1))  # == cos(x).*cos(y).*...
#@vectorize_1arg Any manusol
# derivative for Neumann BC
dmanusol_dn(coords) = -abs(sq1(-sin(coords[1,:]).*prod(map(cos, coords[2:end,:]),1) )) # == - abs(sin(x).*cos(y).*...)
#@vectorize_1arg Any dmanusol_dx
# forcing which produces the manufactured solution
force_fn(coords, tdim) = tdim*manusol(coords)
#@vectorize_1arg Any force_fn

xr = [-pi/2, pi/2]
println("Mesh generation")
@time if dims==1
    ## 1D mesh
    mesh = make_boxmesh(nx, xr; bmark=[2,1])
else
    ## 2D mesh
    # make a bounding polygon
    yr = xr

    bmark = [1;1;1;1];        # give bmark==1 to all nodes to specify Dirichlet
    bmark_facets = [1;2;1;2];   # give a bmark_edge==1 except at x=0 where Neumann BC

    ny = nx+1
    mesh = make_boxmesh(nx, ny, xr, yr; bmark_vertex=bmark, bmark_facet=bmark_facets)
end
vertices,edges,facets,cells = entities(mesh)
println("DOF = $(length(vertices))")


## FEM
f = force_fn(coords(vertices,2), tdims(mesh))

println("FEM operators")
@time begin
# FEM setup
int_nn, int_ne, int_ee, mean_en, Dx_en, Dy_en,
            int_nn_bdy, int_nf_bdy, int_ff_bdy, mean_fn_bdy = SparseFEM.operators(mesh)
end #@time

println("BCs")
@time begin
# active nodes, i.e. not Dirichlet nodes
anodes = iseven(meshfns(vertices)[:bmark])
dirinodes = !anodes;
#number of boundary faces with Neumann BCs: bmark_facet is even
neumann_facets = facets[(meshfns(facets)[:bmark].>0) & (mod(meshfns(facets)[:bmark],2).==0)]
neumann_facets_midpoints = midpoints(neumann_facets)
    
# BC
dirivals = manusol(coords(vertices[dirinodes])) # Dirichlet BC == x
phi = zeros(Float64, length(vertices))
phi[dirinodes] = dirivals
neumannvals = isempty(neumann_facets_midpoints) ? Float64[] : dmanusol_dn(neumann_facets_midpoints)
end #@time

println("Assembly")
@time begin
stiff =   Dx_en[:,anodes].'*int_ee *Dx_en[:,anodes] +
          Dy_en[:,anodes].'*int_ee *Dy_en[:,anodes]
# do BCs by adding contribution of Dirichlet nodes to force vector:
force = int_nn[anodes,:] * f + int_nf_bdy[anodes,:] * neumannvals -
        Dx_en[:,anodes].'*int_ee *[Dx_en[:,dirinodes]*phi[dirinodes]] -
        Dy_en[:,anodes].'*int_ee *[Dy_en[:,dirinodes]*phi[dirinodes]]
end #@time

println("Solve")
@time phi[anodes] = stiff\force;

# absolute maximal error
ms = manusol(coords(mesh,2))
println("Maximum absolute error: $(norm(phi.-ms, Inf))")

if plotyes
    if dims==1
        plot(coords(mesh), phi.-ms)
        # oplot(coords(mesh), ms, "r")
    else
        phir = reshape(phi, ny,nx)
        x   = reshape(coords(mesh)[1,:], ny,nx)
        y   = reshape(coords(mesh)[2,:], ny,nx)
        imagesc(tuple(xr...), tuple(yr...), phir)
        #plot(x, phir[1,:])
    end
end

#end #function
