# Solving a nonlinear PDE, the steady shallow ice equation (a non-linear diffusion
# equation) and compares it to analytic solutions given in [1].  The Picard iteration
# needs to be under-relaxed to work, the scheme used is described in [2].  (Can also be
# solved using fsolve)
#
# div Q = a
# Q = - gamma/(n+2) h^(n+2) * |grad h|^(n-1) grad h
#
# Rescaling x->Lx, y->Ly, h->Zh, a->Aa gives:
# - div (h^(n+2) |grad h|^(n-1) grad h) = a
#
# with ( A(n+2) L^(n+1) )/ (gamma Z^(2n+2)) = 1
#
# References:
# [1] https://www.cs.uaf.edu/~bueler/steadyiso.pdf
# [2] RCA Hindmarsh, AJ Payne - Annals of Glaciology, 1996, page 84

# This m-file is part of the sparseFEM package https://bitbucket.org/maurow/sparsefem
# Copyright (c) 2014, Christian Schoof, Ian J Hewitt & Mauro A Werder
# All rights reserved.
# Licensed under a BSD 2-Clause License, see LICENCE file

using LMesh
using SparseFEM


module M
immutable Dim{D}
    d::Int
    Dim(d) = (@assert D==d; new(d))
end
Dim(d) = Dim{d}(d)
==(d::Dim,o::Integer) = d.d==o
==(o::Integer,d::Dim) = d==o
export Dim
end
using M


dim = Dim(2)
nx=100
plotyes = [false, true][1]

L = 1500e3/2 # domain radius
small_L = 1e-4 # regularization
n = 3.0  # Glen's exponent
g = 9.81 # grav. accel.
rho = 910.0 # density of ice
secpera = 365*24*3600 # sec. per year
A0 = 1.0e-16 / secpera # ice flow constant
gamma = 2 * A0 * (rho * g)^n

hmax = 3500.  # maximal ice thickness

# scaling
Z = hmax
A = gamma * Z^(2*n+2)/((n+2)*L^(n+1))

regularization = 100*eps(1.0)

tol = 1e-3
maxiter = 1000

## Mesh
if dim==1
    ## 1D mesh
    println("1D Mesh generation")
    @time mesh = make_boxmesh(nx, [-1,1]; bmark=[1,1])
else
    ## 2D mesh
    using MAT
    println("2D Mesh loading")
    mesh = mattri_mesh( joinpath(dirname(Base.source_path()), "circlemesh.mat"))
end
# A few mesh helpers
mfs = meshfns(mesh)
xy = coords(mesh) # always a Matrix
vertices,edges,facets,cells = entities(mesh)
println("DOF = $(length(vertices))")

# FEM operators:
int_nn, int_ne, int_ee, mean_en, Dx_en, Dy_en, int_nn_bdy, int_nf_bdy, int_ff_bdy, mean_fn_bdy = SparseFEM.operators(mesh)

## Analytical solution from Bueler:
# https://www.cs.uaf.edu/~bueler/steadyiso.pdf
sq1(x) = squeeze(x, 1)
sq2(x) = squeeze(x, 2)
xy2r(xy) = sq1(sqrt(sum(xy.^2,1)))
const Ln = L*(1+small_L);
if dim==1
    const C1 = 8/3*(5/gamma)^(1/3)
    const alpha = (2*hmax^(8/3)/(C1*L))^3
    const beta = L^(-1/3)
else
    const C1 = (2+2/n)*( (n+2)/gamma )^(1/n)
    const alpha = ((1+1/n) * hmax^(2+2/n)/(C1 *L *(1-1/n)))^n
end
# ice thickness
function h_fn(xy, ::Dim{1})
    x = xy2r(xy)
    hmax * ( 1 + 2*x/Ln - 3/2*(x/Ln).^(4/3) + 3/2*((1-x/Ln).^(4/3) -1)).^(3/8)
end
function h_fn(xy, ::Dim{2})
    r = xy2r(xy)
    (hmax * ( 1 - n/(n-1) * ( (r/Ln).^(1+1/n) -
                             (1-r/Ln).^(1+1/n) + 1 -
                             (1+1/n)*r/Ln) ).^(n/(2*n+2)))
end
# ice flux
function Q_fn(xy, ::Dim{1})
    x = xy2r(xy)
    sign(sq1(xy)).* alpha .* ( beta*x.^(1/3) + beta *(Ln-x).^(1/3) -1 ).^3
end
function Q_fn(xy, ::Dim{2})
    # flux in radially outward direction
    r = xy2r(xy)
    (alpha * ( (r/Ln).^(1/n) + (1-r/Ln).^(1/n) - 1).^n)
end
# accumulation
function a_fn(xy, ::Dim{1})
    x = xy2r(xy)
    origin = find(x.==0)
    x[origin] = L
    out = (alpha/Ln *( (x/Ln).^(1/3) + (1-x/Ln).^(1/3) -1 ).^2 .* 
           ( (x/Ln).^(-2/3) - (1-x/Ln).^(-2/3) ))
    out[origin] = -out[origin]
    return out
end
function a_fn(xy, ::Dim{2})
    r = xy2r(xy)
    (alpha./r .* ( (r/Ln).^(1/n) + (1-r/Ln).^(1/n) -1 ).^n 
     + alpha/Ln * ( (r/Ln).^(1/n) + (1-r/Ln).^(1/n) -1 ).^(n-1) 
     .* ( (r/Ln).^(1/n-1) - (1-r/Ln).^(1/n-1)))
end
              
# Get forcing (accumulation) from analytic solution:
aana = a_fn(L*xy, dim)
hana = h_fn(L*xy, dim) # analytic solution which goes with a
Qana = Q_fn(L*xy, dim) # analytic flushow L;x Q

Qana_mean = mean_en*Qana
# scale analytic forcing
a = aana/A

# active nodes, i.e. not Dirichlet nodes
anodes = iseven(mfs[0][:bmark])
dirinodes = !anodes
#number of boundary faces with Neumann BCs: bmark_facet is even
neumann_facets = facets[(meshfns(facets)[:bmark].>0) & (iseven(meshfns(facets)[:bmark]))]

# BC
dirivals = find(dirinodes)*0
h = zeros(length(vertices))
h[dirinodes] = dirivals
neumannvals = 0* ones(length(neumann_facets),1)

# IC: the solver needs a fairly good IC!
if dim==1
    h[:] = cos(xy*pi/2)
else
    h[:] = cos(xy2r(xy)*pi/2)
end


function shallow_ice_diffusifity(h, n, Dx_en, Dy_en, mean_en, reg)
    # diffuse = shallow_ice_diffusifity(h, n, Dx_en, Dy_en, mean_en, reg)
    #
    # Calculates the scaled effective diffusifity on the elements:
    #  h^(n+2) * |grad h + reg|^(n-1) 
    #
    # Adds a regularization: reg
    grad_hx = Dx_en*h
    grad_hy = Dy_en*h
    abs_grad_h = sqrt(grad_hx.^2 + grad_hy.^2)
    h_mean = mean_en*h # h on elements
    
    diffuse = sq2(h_mean.^(n+2) .* (abs_grad_h + reg).^(n-1))

    return  diffuse, grad_hx, grad_hy
end

## solve by a under-relaxed Picard iteration

# These stay constant during the iteration:
force_const = int_nn[anodes,:] * a + int_nf_bdy[anodes,:] * neumannvals
stiffx_const = Dx_en[:,anodes].'*int_ee
stiffy_const = Dy_en[:,anodes].'*int_ee

delta_h_old = copy(h)*0+0.1 # this is critical
h_old = copy(h)

ii =1
stiff=1
force=1
diffuse=1
for ii=1:maxiter
    # assemble
    diffuse, grad_hx, grad_hy = shallow_ice_diffusifity(h, n, Dx_en, Dy_en, mean_en, regularization)
    Qx = copy(Dx_en[:,anodes])
    Qy = copy(Dy_en[:,anodes])
    broadcast!(*, Qx, diffuse, Dx_en[:,anodes]) # https://github.com/JuliaLang/julia/issues/11474#issuecomment-106674790
    broadcast!(*, Qy, diffuse, Dy_en[:,anodes])
    stiff = stiffx_const * Qx + stiffy_const * Qy
    force = (force_const - 
           (stiffx_const *(diffuse .* (Dx_en[:,dirinodes]*h[dirinodes,:])) 
            + stiffy_const *(diffuse .* (Dy_en[:,dirinodes]*h[dirinodes,:])))
             )
    # solve linear system
    h[anodes] = stiff\force
    h[dirinodes] = dirivals
    # first check whether we converged:
    delta_h = h-h_old
    if ii==10000 
        println(h)
        println(delta_h)
        println(full(stiff))
        println(force)
        error("")
    end
    err = norm(delta_h, Inf)
    if err < tol
        break
    end
    if  any(isnan(h)) || any(isinf(h))
        warn("Encountered NaN or Inf, stopping")
        break
    end
    # RCA Hindmarsh, AJ Payne - Annals of Glaciology, 1996, page 84
    # have a clever way to under-relax the iteration:
    fac = norm(delta_h-delta_h_old, 2)/norm(delta_h_old, 2)
    delta_h = delta_h/fac
    # update h
    h = h_old + delta_h
    h_old[:] = h
    delta_h_old[:] = delta_h
end
if ii==maxiter || any(isnan(h)) || any(isinf(h))
    println("NOT CONVERGED after $ii Picard iterations. Numerical error = $(norm(h-h_old, Inf))")
else
    println("Converged after $ii Picard iterations. Numerical error = $(norm(h-h_old, Inf))")
end

if dim==1
    using Winston
    plot(xy*L/1e3, h*Z, ".-")
    oplot(xy*L/1e3, hana, ".-r")
    xlabel("x (km)")
else
    using Winston
    r = xy2r(xy)
    perm = sortperm(r)
    plot(r[perm]*L/1e3, h[perm]*Z, ".")
    oplot(r[perm]*L/1e3, hana[perm], "r.")
    xlabel("Radius (km)")
end
ylabel("Ice thickness (m)")

## Solve using residual and fsolve
# $$$ resi = @(h) shallow_ice_residual(h, n, a, Dx_en, Dy_en, int_ee, int_nn, mean_en, dirinodes, dirivals, regularization)
# $$$ opts = optimoptions("fsolve", "TolX", 1e-3)
# $$$ h_res = fsolve(resi, hana*0.1, opts)
# $$$ norminf = norm(hana/Z-h_res, Inf)
# $$$ plot(h_res-hana/Z)
