# SparseFEM

This is a port of the matlab
[sparseFEM](https://bitbucket.org/maurow/sparsefem) package, which
does first order finite elements in 1D and 2D.  It works together with
[LMesh](https://bitbucket.org/maurow/lmesh.jl).

Installation
------------
```
Pkg.clone("git@bitbucket.org:maurow/ragged.jl.git", "Ragged")
Pkg.clone("git@bitbucket.org:maurow/lmesh.jl.git", "LMesh")
Pkg.clone("git@bitbucket.org:maurow/sparsefem.jl.git", "SparseFEM")
```
The explicit directory naming is necessary because bitbucket uses
lowercase directories.

