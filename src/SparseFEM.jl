module SparseFEM
using LMesh

import Base: iseven
@vectorize_1arg Int iseven
# all zero sparse matrix (add to Base?) 
Base.SparseMatrix.sparse{Tv<:Number, Ti<:Integer}(::Type{Tv}, ni::Ti, nj::Ti) = sparse(Ti[],Ti[],Tv[],ni,nj) 


function operators(mesh::Mesh; trapezoidal=false, networkops=false)
    # helper fns
    sq1(x) = squeeze(x, 1)
    sq2(x) = squeeze(x, 2)

    # setup
    dimension = tdims(mesh)     #dimensionality of domain
    vertices, edges, facets, cells = entities(mesh)
    n_vertices = length(vertices)
    n_facets = length(facets)
    n_cells = length(cells)           #number of elements
    x = coords(mesh)                         #array of node locations, size n_cells-by-dimension
    connect = con0(cells)                 #n_cells-by-(dimension+1)
    connect_facet = con0(facets)
    # dimension agnostic setup

    bmark_facet = meshfns(facets)[:bmark]
    neumann_facets = (bmark_facet.>0) &  iseven(bmark_facet) #number of boundary faces with Neuman BCs: bmark_facet is even
    n_facets_bdy = sum(neumann_facets)
    elements = 1:n_cells
    facets_bdy = 1:n_facets_bdy

    if dimension == 1
        #operators defined on entire mesh
        connect1 = sq1(connect[1,:])
        connect2 = sq1(connect[2,:])
        dx_connect = x[connect2]-x[connect1] # Jacobian
        abs_dx_connect = abs(dx_connect)   # element size
        if trapezoidal 
            #composite trapezoidal rule
            int_nn = sparse([connect1; connect2], [connect1; connect2], [1/2*abs_dx_connect; 1/2*abs_dx_connect], n_vertices,n_vertices)
        else 
            #exact quadrature
            int_nn = sparse([connect1;           connect1;           connect2;           connect2], 
                            [connect1;           connect2;           connect1;           connect2], 
                            [1/3*abs_dx_connect; 1/6*abs_dx_connect; 1/6*abs_dx_connect; 1/3*abs_dx_connect], n_vertices,n_vertices)
        end
        int_ne =  sparse([connect1; connect2], [elements; elements], [1/2*abs_dx_connect; 1/2*abs_dx_connect], n_vertices,n_cells)
        int_ee =  sparse(elements, elements, abs_dx_connect, n_cells, n_cells)
        mean_en = sparse([elements; elements], [connect1; connect2], 1/2*ones(2*n_cells), n_cells,n_vertices)
        Dx_en =   sparse([elements; elements], [connect1; connect2], [-1./dx_connect; 1./dx_connect], n_cells,n_vertices)
        Dy_en =   sparse(Float64, n_cells, n_vertices);  #zero matrix in 1D
        
        # boundary operators
        connect_bdy1 = find(neumann_facets)
        int_nn_bdy  = sparse(connect_bdy1, connect_bdy1, ones(size(connect_bdy1)), n_vertices,n_vertices)
        int_nf_bdy  = sparse(connect_bdy1, facets_bdy, ones(size(connect_bdy1)), n_vertices,n_facets_bdy)
        int_ff_bdy  = sparse(facets_bdy, facets_bdy, ones(n_facets_bdy),   n_facets_bdy,n_facets_bdy)
        mean_fn_bdy = sparse(facets_bdy, connect_bdy1, ones(size(connect_bdy1)), n_facets_bdy,n_vertices)
        
        # network operators, in 1D equivalent to element operators
        if networkops
            if connect_facet==connect
                error("For facets on 1D: mesh.connect==mesh.connect_facet")
            end
            int_nn_facet = int_nn
            int_nf_facet = int_ne
            int_ee_facet = int_ee
            mean_fn_facet =  mean_en
            Ds_fn_facet = Dx_en
        end
    elseif dimension==2
        # operators defined on entire mesh
        connect1 = sq1(connect[1,:])
        connect2 = sq1(connect[2,:])
        connect3 = sq1(connect[3,:])
        x1 = sq1(x[1,connect1]); x2 = sq1(x[1,connect2]); x3 = sq1(x[1,connect3])
        y1 = sq1(x[2,connect1]); y2 = sq1(x[2,connect2]); y3 = sq1(x[2,connect3])
        Jacobian = (x2-x1).*(y3-y1) - (x3-x1).*(y2-y1)   # Jacobian of canonical transformation
        abs_dS_connect = abs(Jacobian)/2   # the area of each element
        if trapezoidal
            #composite trapezoidal rule
            int_nn = sparse([connect1; connect2; connect3], [connect1; connect2; connect3],
                            [1/3*abs_dS_connect; 1/3*abs_dS_connect; 1/3*abs_dS_connect], n_vertices,n_vertices)
        else 
            #exact quadrature
            int_nn = sparse([connect1; connect1; connect1; connect2; connect2; connect2; connect3; connect3; connect3],
                            [connect1; connect2; connect3; connect1; connect2; connect3; connect1; connect2; connect3],
                            [abs_dS_connect/6; abs_dS_connect/12; abs_dS_connect/12; abs_dS_connect/12; abs_dS_connect/6; 
                             abs_dS_connect/12; abs_dS_connect/12; abs_dS_connect/12; abs_dS_connect/6],
                            n_vertices,n_vertices)
        end
        int_ne = sparse([connect1; connect2; connect3], [elements; elements; elements], [1/3*abs_dS_connect; 1/3*abs_dS_connect; 1/3*abs_dS_connect], n_vertices,n_cells)
        int_ee = sparse(elements, elements, abs_dS_connect, n_cells,n_cells)
        mean_en =  sparse([elements; elements; elements], [connect1; connect2; connect3], 1/3*ones(3*n_cells), n_cells,n_vertices)

        Dx_en = sparse([elements; elements; elements], [connect1; connect2; connect3], [(y2-y3)./Jacobian; (y3-y1)./Jacobian; (y1-y2)./Jacobian], n_cells,n_vertices)
        Dy_en = sparse([elements; elements; elements], [connect1; connect2; connect3], [(x3-x2)./Jacobian; (x1-x3)./Jacobian; (x2-x1)./Jacobian], n_cells,n_vertices)

        # boundary operators
        connect_bdy = connect_facet[:,neumann_facets]
        connect_bdy1 = sq1(connect_bdy[1,:])
        connect_bdy2 = sq1(connect_bdy[2,:])
        dl_bdy = sq1(((x[1,connect_bdy2]-x[1,connect_bdy1]).^2+(x[2,connect_bdy2]-x[2,connect_bdy1]).^2).^(1/2)) # facet size, aka facet-Jacobian
        if trapezoidal
            #composite trapezoidal rule
            int_nn_bdy = sparse([connect_bdy1; connect_bdy2],[connect_bdy1; connect_bdy2],[1/2*dl_bdy; 1/2*dl_bdy],n_vertices,n_vertices)
        else
            #exact quadrature
            int_nn_bdy = sparse([connect_bdy1; connect_bdy1; connect_bdy2; connect_bdy2], [connect_bdy1; connect_bdy2; connect_bdy1; connect_bdy2],
                                [1/3*dl_bdy; 1/6*dl_bdy; 1/6*dl_bdy; 1/3*dl_bdy], n_vertices,n_vertices)
        end
        int_nf_bdy =  sparse([connect_bdy1; connect_bdy2], [facets_bdy; facets_bdy], [1/2*dl_bdy; 1/2*dl_bdy], n_vertices,n_facets_bdy)
        int_ff_bdy =  sparse(facets_bdy, facets_bdy, dl_bdy, n_facets_bdy,n_facets_bdy)
        mean_fn_bdy = sparse([facets_bdy; facets_bdy], [connect_bdy1; connect_bdy2], 1/2*ones(2*n_facets_bdy), n_facets_bdy,n_vertices)
        
        # Network operators
        if networkops
            connect_facet1 = sq1(connect_facet[1,:])
            connect_facet2 = sq1(connect_facet[2,:])
            facets = 1:n_facets
            dl_facet = sq1(((x[1,connect_facet2]-x[1,connect_facet1]).^2 + (x[2,connect_facet2]-x[2,connect_facet1]).^2 ).^(1/2))
            if trapezoidal
                #composite trapezoidal rule
                int_nn_facet = sparse([connect_facet1; connect_facet2], [connect_facet1; connect_facet2],
                                      [1/2*dl_facet; 1/2*dl_facet], n_vertices,n_vertices)
            else
                #exact quadrature
                int_nn_facet = sparse([connect_facet1; connect_facet1; connect_facet2; connect_facet2], 
                                      [connect_facet1; connect_facet2; connect_facet1; connect_facet2],
                                      [1/3*dl_facet;    1/6*dl_facet;    1/6*dl_facet;    1/3*dl_facet], n_vertices,n_vertices)
            end
            int_nf_facet =  sparse([connect_facet1; connect_facet2], [facets; facets], [1/2*dl_facet; 1/2*dl_facet], n_vertices,n_facets)
            int_ff_facet =  sparse(facets, facets, dl_facet, n_facets,n_facets)
            mean_fn_facet = sparse([facets; facets], [connect_facet1; connect_facet2], 1/2*ones(2*n_facets), n_facets,n_vertices)
            Ds_fn_facet =   sparse([facets; facets], [connect_facet1; connect_facet2], [-1./dl_facet; 1./dl_facet], n_facets,n_vertices)
        end
    end
    if networkops
        return (int_nn, int_ne, int_ee, mean_en, Dx_en, Dy_en,
                int_nn_bdy, int_nf_bdy, int_ff_bdy, mean_fn_bdy,
                int_nn_facet, int_nf_facet, int_ff_facet, mean_fn_facet, Ds_fn_facet)
    else
        return (int_nn, int_ne, int_ee, mean_en, Dx_en, Dy_en,
                int_nn_bdy, int_nf_bdy, int_ff_bdy, mean_fn_bdy)
    end

end # sparseFEM

end # module
